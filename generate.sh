
#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <array_size> <output_file>"
    exit 1
fi

array_size=$1
output_file=$2

# Generate an array of random numbers
random_numbers=$(seq 1 $array_size | shuf)

# Save the array to the output file
echo $array_size > $output_file
echo $random_numbers >> $output_file

echo "Array of size $array_size generated and saved to $output_file."
