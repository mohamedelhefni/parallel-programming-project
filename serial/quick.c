#include <stdio.h>
#include <stdlib.h>

void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

int partition(int arr[], int low, int high) {
    int pivot = arr[high];
    int i = low - 1;

    for (int j = low; j <= high - 1; j++) {
        if (arr[j] <= pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return i + 1;
}

void quickSort(int arr[], int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int readArrayFromFile(const char* filename, int** arr, int* size) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        perror("Error opening file");
        return 0;
    }

    int count;
    fscanf(file, "%d", &count);

    *arr = (int*)malloc(count * sizeof(int));

    for (int i = 0; i < count; i++) {
        fscanf(file, "%d", &(*arr)[i]);
    }

    *size = count;

    fclose(file);

    return 1;
}

int main() {
    const char* filename = "../input.txt"; // Change the filename as needed

    int* arr;
    int n;

    if (!readArrayFromFile(filename, &arr, &n)) {
        return 1; // Exit if there's an error reading from the file
    }

    printf("Original array: ");
    // Limit the printing of a large array to avoid flooding the console
    printArray(arr, n > 20 ? 20 : n);

    quickSort(arr, 0, n - 1);

    printf("Sorted array: ");
    // Limit the printing of a large array to avoid flooding the console
    printArray(arr, n > 20 ? 20 : n);

    // Free dynamically allocated memory
    free(arr);

    return 0;
}

